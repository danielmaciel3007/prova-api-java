package br.com.ApiFornecedor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ApiFornecedor.DTO.ClienteDTO;
import br.com.ApiFornecedor.DTO.PedidoDTO;
import br.com.ApiFornecedor.service.ClienteService;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<ClienteDTO> findClienteById(@PathVariable Long id) {
		ClienteDTO clienteDTO = clienteService.findById(id);
		if (clienteDTO != null) {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public ResponseEntity<List<ClienteDTO>> findAll() {
		List<ClienteDTO> clienteDTO = clienteService.getClientes();
		if (clienteDTO != null) {
			return new ResponseEntity<List<ClienteDTO>>(clienteDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<ClienteDTO>>(clienteDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<ClienteDTO> addPedido(@RequestBody ClienteDTO clienteDTO) {
		clienteDTO = clienteService.addCliente(clienteDTO);
		if (clienteDTO != null) {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.PUT)
	public ResponseEntity<ClienteDTO> editPedido(@RequestBody ClienteDTO clienteDTO) {
		clienteDTO = clienteService.editCliente(clienteDTO);
		if (clienteDTO != null) {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.DELETE)
	public ResponseEntity deleteCliente(@RequestBody ClienteDTO clienteDTO) {
		try{
			clienteService.deleteCliente(clienteDTO);
			return new ResponseEntity(HttpStatus.OK);
		} catch(Exception ex) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
	
}
