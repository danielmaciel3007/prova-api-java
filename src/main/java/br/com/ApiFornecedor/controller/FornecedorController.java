package br.com.ApiFornecedor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ApiFornecedor.DTO.FornecedorDTO;
import br.com.ApiFornecedor.service.FornecedorService;


@RestController
@RequestMapping(value = "/fornecedor")
public class FornecedorController {

	@Autowired
	private FornecedorService _fornecedorService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<FornecedorDTO> findFornecedorById(@PathVariable int id) {
		FornecedorDTO fornecedorDTO = _fornecedorService.getFornecedorById(id);
		if (fornecedorDTO != null) {
			return new ResponseEntity<FornecedorDTO>(fornecedorDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<FornecedorDTO>(fornecedorDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<FornecedorDTO> fornecedorAdd(@RequestBody FornecedorDTO fornecedorDTO) {
		fornecedorDTO = _fornecedorService.saveFornecedor(fornecedorDTO);
		if (fornecedorDTO != null) {
			return new ResponseEntity<FornecedorDTO>(fornecedorDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<FornecedorDTO>(fornecedorDTO, HttpStatus.NOT_FOUND);
		}
	}
}
