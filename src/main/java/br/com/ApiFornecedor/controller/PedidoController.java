package br.com.ApiFornecedor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ApiFornecedor.DTO.PedidoDTO;
import br.com.ApiFornecedor.DTO.ProdutoDTO;
import br.com.ApiFornecedor.service.PedidoService;

@RestController
@RequestMapping(value = "/pedido")
public class PedidoController {

	@Autowired
	private PedidoService _pedidoService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<PedidoDTO> findPedidoById(@PathVariable int id) {
		PedidoDTO pedidoDTO = _pedidoService.getPedidoById(id);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public ResponseEntity<List<PedidoDTO>> findAll() {
		List<PedidoDTO> pedidoDTO = _pedidoService.getPedidos();
		if (pedidoDTO != null) {
			return new ResponseEntity<List<PedidoDTO>>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<PedidoDTO>>(pedidoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<PedidoDTO> addPedido(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = _pedidoService.addPedido(pedidoDTO);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.PUT)
	public ResponseEntity<PedidoDTO> editPedido(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = _pedidoService.editPedido(pedidoDTO);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.DELETE)
	public ResponseEntity deletePedido(@RequestBody PedidoDTO pedidoDTO) {
		try{
			_pedidoService.deletePedido(pedidoDTO);
			return new ResponseEntity(HttpStatus.OK);
		} catch(Exception ex) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
	
}
