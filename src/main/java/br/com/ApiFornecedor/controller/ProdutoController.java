package br.com.ApiFornecedor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ApiFornecedor.DTO.FornecedorDTO;
import br.com.ApiFornecedor.DTO.ProdutoDTO;
import br.com.ApiFornecedor.service.FornecedorService;
import br.com.ApiFornecedor.service.ProdutoService;

@RestController
@RequestMapping(value = "/produto")
public class ProdutoController {
	
	@Autowired
	private ProdutoService _produtoService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProdutoDTO> findProdutoById(@PathVariable int id) {
		ProdutoDTO produtoDTO = _produtoService.getProdutoById(id);
		if (produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/all", method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoDTO>> findAll() {
		List<ProdutoDTO> produtoDTO = _produtoService.getProdutos();
		if (produtoDTO != null) {
			return new ResponseEntity<List<ProdutoDTO>>(produtoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<ProdutoDTO>>(produtoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<ProdutoDTO> addProduto(@RequestBody ProdutoDTO produtoDTO) {
		produtoDTO = _produtoService.addProduto(produtoDTO);
		if (produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.PUT)
	public ResponseEntity<ProdutoDTO> fornecedorEdit(@RequestBody ProdutoDTO produtoDTO) {
		produtoDTO = _produtoService.editProduto(produtoDTO);
		if (produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.DELETE)
	public ResponseEntity<ProdutoDTO> fornecedorDelete(@RequestBody ProdutoDTO produtoDTO) {
		_produtoService.deleteProduto(produtoDTO);
		
		if (produtoDTO != null) {
			return new ResponseEntity(produtoDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity(produtoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
}
