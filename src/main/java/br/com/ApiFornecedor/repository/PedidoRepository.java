package br.com.ApiFornecedor.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.ApiFornecedor.model.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Integer>{

}

