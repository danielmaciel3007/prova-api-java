package br.com.ApiFornecedor.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.ApiFornecedor.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

}
