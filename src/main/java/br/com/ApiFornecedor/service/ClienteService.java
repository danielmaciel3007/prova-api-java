package br.com.ApiFornecedor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ApiFornecedor.DTO.ClienteDTO;
import br.com.ApiFornecedor.DTO.PedidoDTO;
import br.com.ApiFornecedor.model.Cliente;
import br.com.ApiFornecedor.model.Pedido;
import br.com.ApiFornecedor.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	public ClienteDTO findById(Long id) {
		Optional<Cliente> oCliente = clienteRepository.findById(id);
		if (oCliente != null && oCliente.isPresent()) {
			Cliente cliente = oCliente.get();
			ClienteDTO clienteDTO = new ClienteDTO(cliente.getId(), cliente.getNome(), cliente.getCpf());
			return clienteDTO;
		} else {
			return null;
		}
		
	}
	
	public List<ClienteDTO> getClientes() {
		Iterable<Cliente> iClientes = clienteRepository.findAll();
		List<ClienteDTO> cs = new ArrayList<ClienteDTO>();
		
		for(Cliente cliente : iClientes) {
			
			ClienteDTO clienteDTO = new ClienteDTO();
			clienteDTO.setId(cliente.getId());
			clienteDTO.setNome(cliente.getNome());
			clienteDTO.setCpf(cliente.getCpf());
		
			cs.add(clienteDTO);
		}
		
		return cs;
	}
	
	public ClienteDTO addCliente(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getCpf());
		cliente = clienteRepository.save(cliente);
		clienteDTO.setId(cliente.getId());
		return clienteDTO;
	}
	
	public ClienteDTO editCliente(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getCpf());
		cliente = clienteRepository.save(cliente);
		return clienteDTO;
	}
	
	public void deleteCliente(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getCpf());
		clienteRepository.delete(cliente);
	}
	
}
