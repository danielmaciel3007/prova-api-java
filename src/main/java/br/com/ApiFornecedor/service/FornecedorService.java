package br.com.ApiFornecedor.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ApiFornecedor.DTO.FornecedorDTO;
import br.com.ApiFornecedor.model.Fornecedor;
import br.com.ApiFornecedor.repository.FornecedorRepository;


@Service
public class FornecedorService {

	@Autowired
	private FornecedorRepository _fornecedorRepository;
	
	public FornecedorDTO getFornecedorById(int id){
		
		Optional<Fornecedor> oFornecedor = _fornecedorRepository.findById(id);
		
		if(oFornecedor != null && oFornecedor.isPresent()) {
			Fornecedor fornecedor = oFornecedor.get();
			FornecedorDTO fornecedorDTO = new FornecedorDTO(); 
			
			fornecedorDTO.setRazaoSocial(fornecedor.getRazaoSocial());
			fornecedorDTO.setNome(fornecedor.getNome());
			fornecedorDTO.setEndereco(fornecedor.getEndereco());
			fornecedorDTO.setCnpj(fornecedor.getCnpj());
			return fornecedorDTO;
		} else {
			return null;
		}
	}
	
	public FornecedorDTO saveFornecedor(FornecedorDTO fornecedorDTO) {
		
		Fornecedor fornecedor = new Fornecedor();
		
		fornecedor.setId(fornecedorDTO.getId());
		fornecedor.setRazaoSocial(fornecedorDTO.getRazaoSocial());
		fornecedor.setNome(fornecedorDTO.getNome());
		fornecedor.setEndereco(fornecedorDTO.getEndereco());
		fornecedor.setCnpj(fornecedorDTO.getCnpj());
		
		
		fornecedor = _fornecedorRepository.save(fornecedor);
		fornecedorDTO.setId(fornecedor.getId());
		return fornecedorDTO;
	}
}
