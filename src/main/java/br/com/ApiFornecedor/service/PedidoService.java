package br.com.ApiFornecedor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ApiFornecedor.DTO.PedidoDTO;
import br.com.ApiFornecedor.model.Pedido;
import br.com.ApiFornecedor.repository.PedidoRepository;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository _pedidoRepository;
	
	public PedidoDTO getPedidoById(int id) {
		
		Optional<Pedido> oPedido = _pedidoRepository.findById(id);
		
		if(oPedido != null && oPedido.isPresent()) {
			Pedido pedido = oPedido.get();
			PedidoDTO pedidoDTO = new PedidoDTO(); 
			
			
			pedidoDTO.setId(pedido.getId());
			pedidoDTO.setDescricao(pedido.getDescricao());
			pedidoDTO.setQtde(pedido.getQtde());
			pedidoDTO.setValorTotal(pedido.getValorTotal());
			pedidoDTO.setProduto(pedido.getProduto());
			pedidoDTO.setCliente(pedido.getCliente());
			
			return pedidoDTO;
			
		} else {
			
			return null;
		}
	}
	
	public List<PedidoDTO> getPedidos() {
		Iterable<Pedido> iPedidos = _pedidoRepository.findAll();
		List<PedidoDTO> ps = new ArrayList<PedidoDTO>();
		
		for(Pedido pedido : iPedidos) {
			
			PedidoDTO pedidoDTO = new PedidoDTO();
			pedidoDTO.setId(pedido.getId());
			pedidoDTO.setDescricao(pedido.getDescricao());
			pedidoDTO.setQtde(pedido.getQtde());
			pedidoDTO.setValorTotal(pedido.getValorTotal());
			pedidoDTO.setProduto(pedido.getProduto());
			pedidoDTO.setCliente(pedido.getCliente());
		
			ps.add(pedidoDTO);
		}
		
		return ps;
	}
	
	public PedidoDTO addPedido(PedidoDTO pedidoDTO) {
			
		Pedido pedido = new Pedido();
		
		pedidoDTO.setDescricao(pedido.getDescricao());
		pedidoDTO.setQtde(pedido.getQtde());
		pedidoDTO.setValorTotal(pedido.getValorTotal());
		pedidoDTO.setProduto(pedido.getProduto());
		pedidoDTO.setCliente(pedido.getCliente());
		
		pedido = _pedidoRepository.save(pedido);
		pedidoDTO.setId(pedido.getId());
		return pedidoDTO;
	
	}
	
	public PedidoDTO editPedido(PedidoDTO pedidoDTO) {
		
		Pedido pedido = new Pedido();
		
		pedidoDTO.setDescricao(pedido.getDescricao());
		pedidoDTO.setQtde(pedido.getQtde());
		pedidoDTO.setValorTotal(pedido.getValorTotal());
		pedidoDTO.setProduto(pedido.getProduto());
		pedidoDTO.setCliente(pedido.getCliente());
		
		pedido = _pedidoRepository.save(pedido);
		
		return pedidoDTO;
	
	}
	
	public void deletePedido(PedidoDTO pedidoDTO) { 
		
		Pedido pedido = new Pedido();
		pedido.setId(pedidoDTO.getId());
		pedido.setDescricao(pedidoDTO.getDescricao());
		pedido.setQtde(pedidoDTO.getQtde());
		pedido.setValorTotal(pedidoDTO.getValorTotal());
		pedido.setProduto(pedidoDTO.getProduto());
		pedido.setCliente(pedidoDTO.getCliente());
		
		_pedidoRepository.delete(pedido);
	}
}
