package br.com.ApiFornecedor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ApiFornecedor.DTO.FornecedorDTO;
import br.com.ApiFornecedor.DTO.ProdutoDTO;
import br.com.ApiFornecedor.model.Fornecedor;
import br.com.ApiFornecedor.model.Produto;
import br.com.ApiFornecedor.repository.FornecedorRepository;
import br.com.ApiFornecedor.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository _produtoRepository;
	private FornecedorService _fornecedorService;
	
	public ProdutoDTO getProdutoById(int id) {
			
		Optional<Produto> oProduto = _produtoRepository.findById(id);
		
		if(oProduto != null && oProduto.isPresent()) {
			Produto produto = oProduto.get();
			ProdutoDTO produtoDTO = new ProdutoDTO(); 
			
			produtoDTO.setId(produto.getId());
			produtoDTO.setNome(produto.getNome());
			produtoDTO.setDescricao(produto.getDescricao());
			produtoDTO.setValor(produto.getValor());

			produto.setFornecedor(produto.getFornecedor());
			
			return produtoDTO;
			
		} else {
			
			return null;
		}
	}
	
	public List<ProdutoDTO> getProdutos() {
		Iterable<Produto> iProdutos = _produtoRepository.findAll();
		List<ProdutoDTO> ps = new ArrayList<ProdutoDTO>();
		for(Produto p : iProdutos) {
			ProdutoDTO pDTO = new ProdutoDTO();
			pDTO.setId(p.getId());
			pDTO.setNome(p.getNome());
			pDTO.setDescricao(p.getDescricao());
			pDTO.setValor(p.getValor());
		
			pDTO.setFornecedor(p.getFornecedor());
			ps.add(pDTO);
		}
		
		return ps;
	}
	
	public ProdutoDTO addProduto(ProdutoDTO produtoDTO) {
			
		Produto produto = new Produto();
		
		produto.setId(produtoDTO.getId());
		produto.setNome(produtoDTO.getNome());
		produto.setDescricao(produtoDTO.getDescricao());
		produto.setValor(produtoDTO.getValor());
		produto.setFornecedor(produtoDTO.getFornecedor());
		
		produto = _produtoRepository.save(produto);
		produtoDTO.setId(produto.getId());
		return produtoDTO;
	
	}
	
	public ProdutoDTO editProduto(ProdutoDTO produtoDTO) {
		
		Produto produto = new Produto();
		
		produto.setId(produtoDTO.getId());
		produto.setNome(produtoDTO.getNome());
		produto.setDescricao(produtoDTO.getDescricao());
		produto.setValor(produtoDTO.getValor());
		produto.setFornecedor(produtoDTO.getFornecedor());
		
		_produtoRepository.save(produto);
		
		return produtoDTO;
	}
	
	public void deleteProduto(ProdutoDTO produtoDTO) { 
		
		Produto produto = new Produto();
		produto.setId(produtoDTO.getId());
		produto.setNome(produtoDTO.getNome());
		produto.setDescricao(produtoDTO.getDescricao());
		produto.setValor(produtoDTO.getValor());
		produto.setFornecedor(produtoDTO.getFornecedor());
		
		_produtoRepository.delete(produto);
	}
}
